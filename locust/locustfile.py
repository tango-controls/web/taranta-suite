from locust import HttpUser, task, between, events
import json
import threading
import asyncio
import websockets
import psutil
import time
from flask import Flask, jsonify, render_template
import docker
import os

# Function to get the PIDs of the TangoGQL container processes
def get_tango_gql_pids(container_name):
    client = docker.from_env()
    try:
        container = client.containers.get(container_name)
        # Get the list of processes inside the container
        processes = container.top()["Processes"]
        # Extract PIDs from the process list
        pids = [int(process[1]) for process in processes if "python" in process[-1]]
        return pids
    except docker.errors.NotFound:
        print(f"Error: Container '{container_name}' not found.")
        return []
    except Exception as e:
        print(f"An error occurred: {e}")
        return []

class TangoGQLUser(HttpUser):
    wait_time = between(0.5, 0.8)
    token = None
    host = "http://localhost:5004"  # Base URL for your TangoGQL application

    def on_start(self):
        self.authenticate()
        if self.token:
            self.start_subscription()
        self.start_resource_monitoring()

    def on_stop(self):
        self.stop_resource_monitoring()

    def authenticate(self):
        auth_payload = {"username": "user1", "password": "abc123"}
        response = self.client.post(
            "http://localhost:22484/auth/login", json=auth_payload
        )
        if response.status_code == 200:
            self.token = response.json().get("taranta_jwt")
            print(f"Authenticated successfully. Token: {self.token}")
        else:
            print(f"Authentication failed: {response.text}")

    @task
    def test_graphql_query(self):
        if self.token:
            query = """
            query {
              device(name:"sys/tg_test/1"){
                state,
                connected,
                alias,
                properties {
                  name
                  device
                }
              }
            }
            """
            headers = {
                "Content-Type": "application/json",
                "Cookie": f"taranta_jwt={self.token}",
            }
            response = self.client.post(
                f"{self.host}/db", json={"query": query}, headers=headers
            )
            if response.status_code != 200:
                print(f"Query failed: {response.status_code} - {response.text}")
            else:
                print(f"Query succeeded: {response.json()}")
        else:
            print("No token available, skipping query.")

    @task
    def test_graphql_mutation(self):
        if self.token:
            mutation = """
            mutation {
            setAttributeValue(device: "sys/tg_test/1", name:"ampli",value:2){
                ok,
                message,
                valueBefore
            }
            }
            """
            headers = {
                "Content-Type": "application/json",
                "Cookie": f"taranta_jwt={self.token}",
            }
            response = self.client.post(
                f"{self.host}/db", json={"query": mutation}, headers=headers
            )
            if response.status_code != 200:
                print(f"Mutation failed: {response.status_code} - {response.text}")
            else:
                print(f"Mutation succeeded: {response.json()}")
        else:
            print("No token available, skipping mutation.")

    def start_subscription(self):
        async def subscribe():
            uri = "ws://localhost:22484/testdb/socket"
            try:
                async with websockets.connect(
                    uri, extra_headers={"Cookie": f"taranta_jwt={self.token}"}
                ) as websocket:
                    await websocket.send(
                        json.dumps({"type": "connection_init", "payload": {}})
                    )
                    await websocket.send(
                        json.dumps(
                            {
                                "id": "1",
                                "type": "start",
                                "payload": {
                                    "query": """
                            subscription{
                              attributes(fullNames: ["sys/tg_test/1/state"]) {
                                value,
                                timestamp,
                                quality
                              }
                            """
                                },
                            }
                        )
                    )
                    while True:
                        response = await websocket.recv()
                        print(f"Subscription update: {response}")
            except Exception as e:
                print(f"WebSocket connection error: {e}")

        def run_subscription(loop):
            asyncio.set_event_loop(loop)
            loop.run_until_complete(subscribe())

        new_loop = asyncio.new_event_loop()
        thread = threading.Thread(target=run_subscription, args=(new_loop,))
        thread.start()

    def start_resource_monitoring(self):
        self.monitoring = True
        self.monitor_thread = threading.Thread(target=self.log_resource_usage)
        self.monitor_thread.start()

    def stop_resource_monitoring(self):
        self.monitoring = False
        self.monitor_thread.join()

    def log_resource_usage(self):
        container_name = "tangogql"  # Replace with your container name
        pids = get_tango_gql_pids(container_name)
        if not pids:
            print("Unable to find the container or retrieve its PIDs.")
            return
        processes = [psutil.Process(pid) for pid in pids]
        while self.monitoring:
            total_cpu_usage = sum(
                process.cpu_percent(interval=1) for process in processes
            )
            total_memory_usage = (
                sum(process.memory_info().rss for process in processes) / 1024**2
            )
            self.update_stats(total_cpu_usage, total_memory_usage, len(processes))
            time.sleep(1)

    def update_stats(self, cpu, memory, num_processes):
        # Update global variables to be served by Flask
        if len(cpu_usage) >= 5000:
            cpu_usage.pop(0)
            memory_usage.pop(0)
            timestamps.pop(0)
            process_counts.pop(0)

        cpu_usage.append(cpu)
        memory_usage.append(memory)
        timestamps.append(time.time())
        process_counts.append(num_processes)


# Flask app to serve the resource usage data
app = Flask(__name__)

# Global variables to store resource usage data
cpu_usage = []
memory_usage = []
timestamps = []
process_counts = []

def get_system_info():
    return {
        "cpu_count": psutil.cpu_count(logical=True),
        "total_memory": psutil.virtual_memory().total / 1024**2  # Convert bytes to MB
    }

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/data")
def data():
    system_info = get_system_info()
    return jsonify(
        {
            "cpu": cpu_usage,
            "memory": memory_usage,
            "timestamps": timestamps,
            "process_counts": process_counts,
            "cpu_count": system_info["cpu_count"],
            "total_memory": system_info["total_memory"]
        }
    )

def run_flask():
    app.run(port=8090)

# Start the Flask server in a separate thread
flask_thread = threading.Thread(target=run_flask)
flask_thread.start()

if __name__ == "__main__":
    os.system("locust -f locustfile.py")
